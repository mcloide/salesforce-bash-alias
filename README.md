# Salesforce Bash Alias

## Getting started

This is an alias for the Salesforce most used command lines.

The available commands are
- **createLightningApp**: Will create a default structure for a Salesforce Lightning App. Name is required for this command.
- **createDXProject**: Will create a default structure for a Salesforce DX project. Name is required for this command.
- **createComponent**: Will create either a LWC or Aura. Name and Type are required for this command. Type will default to lwc.
- **login**: Will Login, through web flow, to a specified Org.
- **deploy**: Will deploy your project to the connected Org.

This assumes that the **sf** sdk for command line is installed.

## Requirements

- Salesforce CLI - [Install](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_install_cli.htm)
- Salesforce Sandbox or Production Environment - [Sandbox Help](https://help.salesforce.com/s/articleView?id=sf.data_sandbox_create.htm&type=5)
- Terminal that supports Bash Script execution - [How to Run a Bash Script](https://devconnected.com/how-to-run-a-bash-script/)

## Installation
Clone to repo into a given location, make it executable and add it to 
the $PATH environment variable. This will allow the script to be executed from anywhere.
For more information on how to install a bash script, please [read](https://askubuntu.com/questions/153251/launch-shell-scripts-from-anywhere)
this Q&A.

**Note**: The script assumes that is being invoked on the correct directory.

## Contributing
To contribute, fork the repo and then create a [Pull Request](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/creating-a-pull-request-from-a-fork). 
After review, it will 
be merged. Even knowing that is a bash script it is expected all the coding standards
from any other language.

## License
I believe in Open Source as a developer, I'm is dedicated to open source. 
Not only do I believe in it, but we use it, and we give back to it. 
This project is under [GPL](https://www.gnu.org/licenses/licenses.en.html) 
License.

## Project status
Up-to-date.