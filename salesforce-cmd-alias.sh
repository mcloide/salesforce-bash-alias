#!/usr/bin/env bash

# author: Cristiano Diniz da Silva <mcloide1977@Gitlab|mcloide@Github>
# Simple bash script that will make an alias to the most used commands for Salesforce command line.
# Yes! This is an alias for an alias. The Salesforce commands are just too big to remember all.

# Will define the command name that will be requested from the available functions above. Required.
COMMAND_NAME=""
# Will define the second required parameter when a command is passed. For project and component, for example, the name. Optional.
COMMAND_ARG1=""
# Will define the third required parameter when a command is passed. For component, for example, the type. Optional.
COMMAND_ARG2=""

# Will read all of the options passed through command line and parse them accordingly, where:
# a is the command name
# b is the second required argument
# c is the third required argument
while getopts "a:b::c::" opt; do
  case $opt in
    a)
      COMMAND_NAME=$OPTARG
      if [ -z "$COMMAND_NAME" ]
      then
        echo "Error: You must pass at least one command to be executed."
        exit 1
      fi
    ;;
    b)
      requiredParamsCommands=["createDXProject","createCommand","createLightningApp"]
      COMMAND_ARG1=$OPTARG

      if [[ ${requiredParamsCommands[@]} =~ $COMMAND_NAME ]]
      then
        if [ -z "$COMMAND_ARG1"]
        then
            echo "Error: For project or component creation a name is required."
            exit 1
        fi
      fi
    ;;
    c)
      COMMAND_ARG1=$OPTARG
      requiredParamsCommands=["createCommand"]

      if [[ ${requiredParamsCommands[@]} =~ $COMMAND_NAME ]]
      then
        if [ -z "$COMMAND_ARG2"]
        then
            echo "Error: For component creation a type is required. Available types: lwc or aura."
            exit 1
        fi
      fi
    ;;
    *)
      documentation
      exit 1
    ;;
  esac
done

# Will check if the prior command executed was successful, otherwise throw an error message and exit.
# @throws Exception
# return void
function checkForErrors () {
    # check if the command was successful, otherwise, display an error message.
    status=$?
    if [ $status -eq 1 ]
    then
      echo "Error executing the sf command. Please see errors above."
      exit 1
    fi
}

# Will create a component using the passed arguments from the command line.
# @param COMMAND_ARG1 as component name
# @param COMMAND_ARG2 as type. Defaults to lwc.
# @throws Exception when missing information
# return void
function createComponent () {

  if [ -z "$COMMAND_ARG2" ]
  then
    COMMAND_ARG2='lwc'
  fi

  sf lightning generate component -n $COMMAND_ARG1 -d force-app/main/default/lwc --type $COMMAND_ARG2

  checkForErrors
}

# Will create  lightning app using the passed arguments from the command line.
# @param COMMAND_ARG1 as component name
# @throws Exception when missing information
# return void
function createLightningApp () {
  sf lightning generate app --name $COMMAND_ARG1

  checkForErrors
}

# Will create a DX Project using the passed arguments from the command line.
# @param COMMAND_ARG1 as component name
# @throws Exception when missing information
# return void
function createDXProject () {
  sf project generate --name $COMMAND_ARG1

  checkForErrors
}

# Will login to an org using the web flow.
# @throws Exception when missing information
# return void
function login () {
  sf org login web
}

# Will quickly deploy a validated deployment to an org.
# @throws Exception when missing information
# return void
function deploy () {
  sf project deploy pipeline quick
}

# Will generate and display a beautifully parsed documentation for the user.
function documentation() {
    echo "+-------------------------------------------------------------------------------------------------------------------------------+"
    echo "| createLightningApp: Will create a default structure for a Salesforce Lightning App. Name is required for this command.        |"
    echo "| Example: createLightningApp myNewSalesForceLightningApp                                                                       |"
    echo "|                                                                                                                               |"
    echo "| createDXProject: Will create a default structure for a Salesforce project. Name is required for this command.                 |"
    echo "| Example: createDXProject myNewSalesForceProject                                                                               |"
    echo "|                                                                                                                               |"
    echo "| createComponent: Will create either a LWC or Aura. Name and Type are required for this command. Type will default to LWC.     |"
    echo "| Examples:                                                                                                                     |"
    echo "| - createComponent myNewComponent lwc                                                                                          |"
    echo "| - createComponent myNewComponent aura                                                                                         |"
    echo "| - createComponent myNewComponent                                                                                              |"
    echo "|                                                                                                                               |"
    echo "| login: Will Login to a specified Org.                                                                                         |"
    echo "|                                                                                                                               |"
    echo "| deploy: Will deploy your project to the connected Org.                                                                        |"
    echo "+-------------------------------------------------------------------------------------------------------------------------------+"
}

# Will display a detailed information header for the user so the invocation of the commands are correct.
function displayHeader () {
  echo "+-------------------------------------------------------------------------------------------------------------------------------+"
  echo "| This is an alias for the Salesforce most used command lines.                                                                  |"
  echo "| The available commands are:                                                                                                   |"
  echo "| - createLightningApp: Will create a default structure for a Salesforce Lightning App. Name is required for this command.      |"
  echo "| - createDXProject: Will create a default structure for a Salesforce DX project. Name is required for this command.            |"
  echo "| - createComponent: Will create either a LWC or Aura. Name and Type are required for this command. Type will default to LWC.   |"
  echo "| - login: Will Login to a specified Org.                                                                                       |"
  echo "| - deploy: Will deploy your project to the connected Org.                                                                      |"
  echo "+-------------------------------------------------------------------------------------------------------------------------------+"
  echo "| This assumes that the sf sdk for command line is installed.                                                                   |"
  echo "+-------------------------------------------------------------------------------------------------------------------------------+"

  documentation
}

# Displaying the general information for the user
displayHeader

# Invoking the required command function
if [ -z "$COMMAND_NAME" ]
then
  echo ""
  echo "ERROR: Invalid command."
  echo ""
  exit 1
fi

echo ""
$COMMAND_NAME